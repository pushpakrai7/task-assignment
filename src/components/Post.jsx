import  { useState, useEffect } from 'react';
import {
  Container,
  TextField,
  CircularProgress,
  AppBar,
  Toolbar,
  Typography,
  Card,
  CardContent,
  Grid,
  CssBaseline,
  Button,
  Box,
  CardActions,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import {fetchPosts} from '../services/api'

const PREFIX = 'App';

const classes = {
  appBar: `${PREFIX}-appBar`,
  searchBar: `${PREFIX}-searchBar`,
  card: `${PREFIX}-card`,
  cardContent: `${PREFIX}-cardContent`,
  cardActions: `${PREFIX}-cardActions`,
  noResults: `${PREFIX}-noResults`,
};

const Root = styled('div')(({ theme }) => ({
  [`& .${classes.appBar}`]: {
    marginBottom: theme.spacing(4),
    backgroundColor: '#4caf50',
  },
  [`& .${classes.searchBar}`]: {
    marginBottom: theme.spacing(4),
  },
  [`& .${classes.card}`]: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)',
    transition: '0.3s',
    '&:hover': {
      boxShadow: '0 8px 16px 0 rgba(0, 0, 0, 0.2)',
    },
  },
  [`& .${classes.cardContent}`]: {
    flexGrow: 1,
  },
  [`& .${classes.cardActions}`]: {
    justifyContent: 'flex-end',
  },
  [`& .${classes.noResults}`]: {
    textAlign: 'center',
    color: 'red',
    padding: theme.spacing(2),
    border: '1px solid red',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#ffe6e6',
  },
}));

const Post = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const postsData = await fetchPosts();
        setData(postsData);
        setFilteredData(postsData);
        setLoading(false);
      } catch (error) {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  const handleSearch = (event) => {
    const search = event.target.value.toLowerCase();
    setSearchTerm(search);
    setFilteredData(data.filter((item) => item.title.toLowerCase().includes(search)));
  };

  return (
    <Root>
      <CssBaseline />
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h4" style={{ flexGrow: 1 }}>
            Post List
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="lg">
        <TextField
          label="Search Posts"
          variant="outlined"
          fullWidth
          value={searchTerm}
          onChange={handleSearch}
          margin="normal"
          className={classes.searchBar}
        />
        {loading ? (
          <Box display="flex" justifyContent="center" alignItems="center" height="60vh">
            <CircularProgress />
          </Box>
        ) : (
          <Grid container spacing={4}>
            {filteredData.length === 0 ? (
              <Grid item xs={12}>
                <Box className={classes.noResults}>
                  <Typography variant="h6">No results found.</Typography>
                </Box>
              </Grid>
            ) : (
              filteredData.map((item) => (
                <Grid item xs={12} sm={6} md={4} key={item.id}>
                  <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                      <Typography variant="h6" component="h2">
                        {item.title}
                      </Typography>
                      <Typography color="textSecondary">{item.body}</Typography>
                    </CardContent>
                    <CardActions className={classes.cardActions}>
                      <Button variant="contained" color="primary">
                        Learn More
                      </Button>
                    </CardActions>
                  </Card>
                </Grid>
              ))
            )}
          </Grid>
        )}
      </Container>
    </Root>
  );
};

export default Post;
